# Découverte Git

## Les titres

# titre de niveau 1

## titre de niveau 2

### etc

... titre

###### titre de niveau 6

```md
# titre de niveau 1
## titre de niveau 2
### etc...
...
###### titre de niveau 6
```

```py
print("Poulet")
```

## Les commandes

Avec les magic quotes (alt-gr + è)

Lancer un programme en Python: `py script.py`

```md
Lancer un programme en Python: `py script.py`
```

## Les notes

> Ceci est une note, elle attire l'attention sur quelque chose à prendre en compte comme une option.

```md
> Ceci est une note, elle attire l'attention sur quelque chose a prendre en compte comme une option.
```

## Les liens (ou ancres)

Permet la navigation (interne ou externe).

Voir [le site de Python.](https://www.python.org/)

```md
Voir [le site de Python.](https://www.python.org/)
```

Lien vers le [support de git](./git-base.pdf).

## Les images

Comme pour les ancres mais avec un point d'exclamation devant.

![Image de bord de mer](https://images.unsplash.com/photo-1624220684521-b4de9fbff4a7?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80)

![texture](./texture.jpg)

```md
![](https://images.unsplash.com/photo-1624220684521-b4de9fbff4a7?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80)
```

## Les tableaux

| id | nom | prenom |
|----|-----|--------|
|1|Dupond|Jean|
|2|Dupont|Jean|

## Les éléments de texte

### Le gras ou emphase

Pour mettre des éléments en **valeur**

```md
Pour mettre des éléments en **valeur**
```

### L'italique

Pour attirer l'attention sur un *paramètre* particulier.

```md
Pour attirer l'attention sur un *paramètre* particulier.
```

## Les listes

### Liste non ordonnées

Pratique pour l'énumération

- Faire les courses
- trouver une place pour se stationner
- être à l'heure
  - si possible avec les croissants

### Les listes ordonnées

Pratique pour ordonner des éléments.

Il est possible de faire des sous-listes. Il faut donc repartir à 1 dans la numérotation.

1) France
2) Belgique
3) Pays-bas
2) Allemagne
    1) Bayern de Munich
    1) Dortmund

```md
1) France
2) Belgique
3) Pays-bas
2) Allemagne
    1) Bayern de Munich
    1) Dortmund
```

# Initialiser un repo git. Ça crée un fichier .git qui contient le récap des modifs
git init

# Liste de la config de git
git config --list
# Rajouter l'adresse mail:
git config --global user.mail "adresse_mail"
# Rajouter le user
git config --user.name "nom_de_l_utilisateur"

# Commenter l'ajout de contenu
git commit -m "message de test"

# Afficher le commentaire
git log

# ajouter un fichier de commentaires
git add <nom du fichier>

# Demander à afficher l'URL de lien du projet
git remote get-utl origin

# Comment mettre à jour le projet en ligne:
git add *
git commit -m "nouveau_message"
git push origin main 
#le git push prends pour 2 variables <remote> <branche à envoyer>



`git remote add <variable du remote> <url du remote>`
Exemple: git remote add 


# force la mise à jour de la comparaison locale/en ligne
git fetch

# Pull: récupérer les commits du remote
git pull

# statut de la mise à jour de la comparaison locale/en ligne
git status



